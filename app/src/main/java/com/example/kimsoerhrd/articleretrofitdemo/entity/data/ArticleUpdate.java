package com.example.kimsoerhrd.articleretrofitdemo.entity.data;

import com.google.gson.annotations.SerializedName;

public  class ArticleUpdate {


    @SerializedName("IMAGE")
    private String IMAGE;
    @SerializedName("STATUS")
    private String STATUS;
    @SerializedName("CATEGORY_ID")
    private int CATEGORY_ID;
    @SerializedName("AUTHOR")
    private int AUTHOR;
    @SerializedName("DESCRIPTION")
    private String DESCRIPTION;
    @SerializedName("TITLE")
    private String TITLE;

    public String getIMAGE() {
        return IMAGE;
    }

    public void setIMAGE(String IMAGE) {
        this.IMAGE = IMAGE;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public int getCATEGORY_ID() {
        return CATEGORY_ID;
    }

    public void setCATEGORY_ID(int CATEGORY_ID) {
        this.CATEGORY_ID = CATEGORY_ID;
    }

    public int getAUTHOR() {
        return AUTHOR;
    }

    public void setAUTHOR(int AUTHOR) {
        this.AUTHOR = AUTHOR;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    @Override
    public String toString() {
        return "ArticleUpdate{" +
                "IMAGE='" + IMAGE + '\'' +
                ", STATUS='" + STATUS + '\'' +
                ", CATEGORY_ID=" + CATEGORY_ID +
                ", AUTHOR=" + AUTHOR +
                ", DESCRIPTION='" + DESCRIPTION + '\'' +
                ", TITLE='" + TITLE + '\'' +
                '}';
    }
}
