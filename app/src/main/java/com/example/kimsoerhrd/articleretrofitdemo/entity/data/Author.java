package com.example.kimsoerhrd.articleretrofitdemo.entity.data;

import com.google.gson.annotations.SerializedName;

public class Author {
    @SerializedName("ID")
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
