package com.example.kimsoerhrd.articleretrofitdemo.entity.data;

import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("NAME")
    private String catName;
    @SerializedName("ID")
    private int catId;

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    @Override
    public String toString() {
        return "Category{" +
                "catName='" + catName + '\'' +
                ", catId=" + catId +
                '}';
    }
}
