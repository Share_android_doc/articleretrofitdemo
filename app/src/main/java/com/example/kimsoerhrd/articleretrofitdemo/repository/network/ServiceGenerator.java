package com.example.kimsoerhrd.articleretrofitdemo.repository.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    final static String BASE_URL="http://api-ams.me/";

    public static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createService(Class<S> service){
        Retrofit retrofit = builder.build();
        return retrofit.create(service);
    }



}
