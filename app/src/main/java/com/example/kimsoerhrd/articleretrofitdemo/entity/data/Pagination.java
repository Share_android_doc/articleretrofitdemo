package com.example.kimsoerhrd.articleretrofitdemo.entity.data;

import com.google.gson.annotations.SerializedName;

public class Pagination {
    @SerializedName("TOTAL_PAGES")
    private int totalPage;
    @SerializedName("TOTAL_COUNT")
    private int countPage;
    @SerializedName("LIMIT")
    private int limitPage;
    @SerializedName("PAGE")
    private int page;

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getCountPage() {
        return countPage;
    }

    public void setCountPage(int countPage) {
        this.countPage = countPage;
    }

    public int getLimitPage() {
        return limitPage;
    }

    public void setLimitPage(int limitPage) {
        this.limitPage = limitPage;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public String toString() {
        return "Pagination{" +
                "totalPage=" + totalPage +
                ", countPage=" + countPage +
                ", limitPage=" + limitPage +
                ", page=" + page +
                '}';
    }
}
