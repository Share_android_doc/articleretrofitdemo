package com.example.kimsoerhrd.articleretrofitdemo.repository;

import com.example.kimsoerhrd.articleretrofitdemo.entity.data.ArticleUpdate;
import com.example.kimsoerhrd.articleretrofitdemo.entity.respone.ArticleRespone;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AMSService {

    interface ArticleService{

        @GET("v1/api/articles")
        Call<ArticleRespone> getArticles();
        @PUT("v1/api/articles/{id}")
        Call<ArticleUpdate> updateArticle(@Path("id") int id, @Body ArticleUpdate articleUpdate);
    }

}
