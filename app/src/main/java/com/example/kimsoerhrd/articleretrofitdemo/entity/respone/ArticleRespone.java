package com.example.kimsoerhrd.articleretrofitdemo.entity.respone;

import com.example.kimsoerhrd.articleretrofitdemo.entity.data.Article;
import com.example.kimsoerhrd.articleretrofitdemo.entity.data.Pagination;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleRespone {

    @SerializedName("PAGINATION")
    private Pagination pagination;
    @SerializedName("DATA")
    private List<Article> articleList;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("CODE")
    private String code;

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public List<Article> getArticleList() {
        return articleList;
    }

    public void setArticleList(List<Article> articleList) {
        this.articleList = articleList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ArticleRespone{" +
                "pagination=" + pagination +
                ", articleList=" + articleList +
                ", message='" + message + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
