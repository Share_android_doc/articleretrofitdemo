package com.example.kimsoerhrd.articleretrofitdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.kimsoerhrd.articleretrofitdemo.entity.data.Article;
import com.example.kimsoerhrd.articleretrofitdemo.entity.data.ArticleUpdate;
import com.example.kimsoerhrd.articleretrofitdemo.entity.respone.ArticleRespone;
import com.example.kimsoerhrd.articleretrofitdemo.repository.AMSService;
import com.example.kimsoerhrd.articleretrofitdemo.repository.network.ServiceGenerator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    AMSService.ArticleService articleService;
    EditText editText;
    ArticleUpdate article;
    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = findViewById(R.id.etTitle);
        textView = findViewById(R.id.tvResult);
        articleService = ServiceGenerator.createService(AMSService.ArticleService.class);

    }

    public void onUpdate(View view) {
        updateArticle();
    }

    void updateArticle(){
        article = new ArticleUpdate();
        article.setTITLE("Love you");
        Call<ArticleUpdate> call = articleService.updateArticle(82614, article);
        call.enqueue(new Callback<ArticleUpdate>() {
            @Override
            public void onResponse(Call<ArticleUpdate> call, Response<ArticleUpdate> response) {

            }

            @Override
            public void onFailure(Call<ArticleUpdate> call, Throwable t) {

            }
        });


    }

    public void onGetArticle(View view) {
        getArticle();
    }

    void getArticle(){
        Call<ArticleRespone> call = articleService.getArticles();
        call.enqueue(new Callback<ArticleRespone>() {
            @Override
            public void onResponse(Call<ArticleRespone> call, Response<ArticleRespone> response) {
                Log.e("error", response.body().getArticleList().toString());

                String data="";
                for (Article article : response.body().getArticleList()){
                    data+= article.getDescription();
                }

                textView.setText(data);

            }

            @Override
            public void onFailure(Call<ArticleRespone> call, Throwable t) {

            }
        });
    }
}
